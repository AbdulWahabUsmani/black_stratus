import 'package:flutter/material.dart';

class Rides extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Rides_State();
  }
}

class Rides_State extends State<Rides> with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            }),
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
        bottom: TabBar(
          tabs: [
            Tab(
              text: 'Upcoming',
            ),
            Tab(
              text: 'Past',
            )
          ],
          controller: _tabController,
        ),
      ),
    );
  }
}
