import 'dart:io';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './rides.dart';
import './help.dart';
import './creditcarddetails.dart';
import '../ui/login.dart';

class Maps extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Maps_State();
  }
}

class Maps_State extends State<Maps> {
  bool _mapMoving = false;
  GoogleMapController _mapController;
  CameraPosition _currentCameraPosition =
      CameraPosition(target: LatLng(26.299188, 50.142641), zoom: 17);


  Future<void> logoutRequest() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    _confirmation();

  }


  Widget _confirmation() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Are you sure you want to logout"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("yes"),
              onPressed: () {
                setState(() {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => Login(),
                      ));
                });
              },
            ),
            FlatButton(
              child: Text("no"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
        backgroundColor: Colors.black,
      ),

      drawer: Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'Stratus Ride',
              style: TextStyle(color: Colors.white),
            ),
            decoration: BoxDecoration(color: Colors.black),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_car),
            title: Text('Rides'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => Rides(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Profile'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text('Payment'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => Payment()));
            },
          ),
          ListTile(
            leading: Icon(Icons.help),
            title: Text('Help'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => Help(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.help),
            title: Text('Logout'),
            onTap: () {
              logoutRequest();
            },
          ),

        ],
      )),
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Stack(
          children: <Widget>[
            GoogleMap(
              rotateGesturesEnabled: false,
              tiltGesturesEnabled: false,
              mapType: MapType.normal,
              initialCameraPosition: _currentCameraPosition,
              onMapCreated: (controller) {
                _mapController = controller;
              },
              compassEnabled: true,
              myLocationEnabled: true,
//              onCameraIdle: () async {
//                print('Camera is idle...');
//                await _lm.setPickupLocation(_currentCameraPosition.target);
//                setState(() {
//                  _mapMoving = false;
//                });
//              },
              onCameraMove: (camPos) {
                _currentCameraPosition = camPos;
              },
              onCameraMoveStarted: () {
                setState(() {
                  _mapMoving = true;
                });
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 4,
                bottom: 360,
              ),
              child: Card(
                color: Colors.transparent,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Pickup Location',
                            labelStyle: TextStyle(color: Colors.black),
                            hintText: 'Enter pickup location',
                            hintStyle: TextStyle(color: Colors.black)),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Dropoff Location',
                            labelStyle: TextStyle(color: Colors.black),
                            hintText: 'Enter dropoff location',
                            hintStyle: TextStyle(color: Colors.black)),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: (_mapMoving) ? Offset(0, -10) : Offset(0, 0),
              child: Center(
                child: Icon(
                  Icons.location_on,
                  size: (_mapMoving) ? 40 : 30,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(0, -45),
              child: Container(
                width: 100,
                height: 30,
                decoration: ShapeDecoration(
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    )),
                child: Center(
                    child: Text(
                  'Pickup Location',
                  style: TextStyle(color: Colors.white, fontSize: 10),
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: Text('Are sure you want to exit'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => exit(0),
                child: Text(
                  "Yes",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  'No',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }
}
