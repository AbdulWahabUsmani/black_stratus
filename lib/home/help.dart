import 'package:flutter/material.dart';
import './help_center.dart';

class Help extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return help_State();
  }
}

class help_State extends State<Help> {
  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            }),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/full_background.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32.0, right: 32.0),
            child: ListView(
              children: <Widget>[
                Text("Help",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold)),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Help_Center()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Help',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 64.0),
                  child: Text("Contact Us",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold)),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Via Chat',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('03332222222',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('02223333333',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 64.0),
                  child: Text("Legal",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold)),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Legal Notice',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Terms & Conditions',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            color: Colors.grey,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Privacy & Policy',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                    //    Expanded(child: RaisedButton(onPressed: (){}, child: Text("Help",textAlign: TextAlign.right,style: TextStyle(color: Colors.white,fontSize: 20.0)),color: Colors.grey,))
                  ],
                ),
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
