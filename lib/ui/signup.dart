import 'package:flutter/material.dart';
import './otp_signup.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:black_stratus/Data/ui_handler.dart';

class SignUp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignUp_State();
  }
}

class Response {
  final String registeredEmail;
  final bool emailStatus;
  final String emailMessage;
  final String alreadyRegisteredMessage;

  Response(
      {this.registeredEmail,
      this.emailStatus,
      this.emailMessage,
      this.alreadyRegisteredMessage});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      registeredEmail: json['registerdEmail'] as String,
      emailStatus: json['emailStatus'] as bool,
      emailMessage: json['emailMessage'] as String,
      alreadyRegisteredMessage: json['alreadyRegisteredMessage'] as String,
    );
  }
}

class SignUp_State extends State<SignUp> {
  TextEditingController emailcontroller = TextEditingController();
  String email;
  static Handler handler = Handler('sendEmail');
  static String url = handler.getUrl();

  Future<Response> emailRequest() async {
    email = emailcontroller.text.toLowerCase();
    if (email.isEmpty) {
      _ifempty();
    } else if (!email.contains("@")) {
      _emailcheck();
    } else {
      var response = await http.post(Uri.encodeFull(url),
          body: json.encode({"userEmailRequest": "$email"}),
          headers: {
            "content-type": "application/json",
            "accept": "application/json"
          });

      var data = json.decode(response.body);
      var emailMessage = data["emailMessage"] as String;
      var emailStatus = data["emailStatus"] as bool;
      var alreadyRegisteredMessage = data["alreadyRegisteredMessage"] as String;

      if (emailStatus == true) {
        setState(() {
          _ifValid(emailMessage);
        });
      } else {
        _ifRegistered(alreadyRegisteredMessage);
      }
    }
  }

  Widget _ifValid(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                sendDataToSecondScreen(context);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifRegistered(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifempty() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please fill out all the fields"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _emailcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please enter a valid email"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            }),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/full_background.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32.0, right: 32),
            child: Form(
              autovalidate: true,
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Padding(
                        padding: const EdgeInsets.only(top: 64.0),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: emailcontroller,
                          style: TextStyle(color: Colors.grey),
                          decoration: InputDecoration(
                              labelText: 'Email',
                              labelStyle: TextStyle(color: Colors.grey),
                              hintText: 'Enter Email',
                              hintStyle: TextStyle(color: Colors.grey),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey)),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              )),
                          cursorColor: Colors.grey,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                        child: RaisedButton(
                          child: Text('Verify',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          color: Colors.amberAccent,
                          onPressed: () {
                            emailRequest();
                            //  Navigator.push(context, MaterialPageRoute(builder: (context)=>otp_SignUp()));
                          },
                        ),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void sendDataToSecondScreen(BuildContext context) {
    String email = emailcontroller.text;
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => otp_SignUp(email),
        ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
