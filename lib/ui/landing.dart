import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import './login.dart';

class Landing extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Landing_State();
  }
}

class Landing_State extends State<Landing> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SplashScreen(
      seconds: 5,
      backgroundColor: Colors.black,
      navigateAfterSeconds: Login(),
      image: Image.asset('images/revisedlogo.png'),
      photoSize: 100.0,
    );
  }
}
