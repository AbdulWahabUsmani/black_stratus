import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './signup.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import '../forgotpassword/forgot_password_email.dart';
import 'package:black_stratus/home/main_page.dart';
import 'package:black_stratus/Data/ui_handler.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Login_State();
  }
}

class Response {
  final String timestamp;
  final int status;
  final String error;
  final String message;
  final String path;
  final String token;

  Response(
      {this.timestamp,
      this.status,
      this.error,
      this.message,
      this.path,
      this.token});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      timestamp: json['timestamp'] as String,
      status: json['status'] as int,
      error: json['error'] as String,
      message: json['message'] as String,
      path: json['path'] as String,
      token: json['token'] as String,
    );
  }
}

class Login_State extends State<Login> {
  static var email = TextEditingController();
  static var password = TextEditingController();
  static Handler handler = Handler('authenticate');
  static String url = handler.getUrl();

  Future<Response> loginRequest() async {
    String useremail = email.text.toLowerCase();
    String pass = password.text;

    if (useremail.isEmpty || pass.isEmpty) {
      _ifempty();
    } else if (!useremail.contains("@")) {
      _emailcheck();
    } else if (pass.length < 8) {
      _passwordcheck();
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String res;
      var response = await http.post(Uri.encodeFull(url),
          body: json.encode({"userEmail": "$useremail", "password": "$pass"}),
          headers: {
            "content-type": "application/json",
            "accept": "application/json"
          });
      var data = json.decode(response.body);
      var message = data["message"] as String;
      if (response.statusCode == 200) {
        setState(() {
          _ifValid();
          res = response.body;
          String responded = res.substring(10, 161);
          prefs.setString('token', "$responded");
        });
      } else {
        _ifNotValid(message);
      }
    }



//    String stringValue = prefs.getString('token');
  }

  Widget _ifValid() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Login Successful"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Maps()));
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifNotValid(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifempty() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please fill out all the fields"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _emailcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please enter a valid email"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _passwordcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content:
              Text("password must be greater then or equal to 8 characters"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: WillPopScope(
    onWillPop: onWillPop,

    child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/full_background.png"),
                fit: BoxFit.cover)),
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: CustomScrollView(
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate([
                  Padding(
                    padding: const EdgeInsets.only(top: 64.0),
                    child: TextFormField(
                      controller: email,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(color: Colors.grey),
                      decoration: InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(color: Colors.grey),
                          hintText: 'Enter email',
                          hintStyle: TextStyle(color: Colors.grey),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          )),
                      cursorColor: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: TextFormField(
                      controller: password,
                      obscureText: true,
                      style: TextStyle(color: Colors.grey),
                      decoration: InputDecoration(
                          fillColor: Colors.grey,
                          labelText: 'Password',
                          labelStyle: TextStyle(color: Colors.grey),
                          hintText: 'Enter password',
                          hintStyle: TextStyle(color: Colors.grey),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          )),
                      cursorColor: Colors.grey,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                        child: GestureDetector(
                          child: Text(
                            'Forgot password ?',
                            style: TextStyle(
                                color: Colors.amberAccent,
                                fontWeight: FontWeight.bold),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        forgot_password_email()));
                          },
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                    child: RaisedButton(
                      child: Text(
                        'Login',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      color: Colors.amberAccent,
                      onPressed: () {
                        loginRequest();
                        //_check();
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                    child: RaisedButton(
                      child: Text('Signup',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      color: Colors.amberAccent,
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SignUp()));
                      },
                    ),
                  ),
                ]),
              )
            ],
          ),
        ),
      ),
      ),);
  }

  Future<bool> onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Are sure you want to exit?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => exit(0),
            child: Text(
              "Yes",
              style: TextStyle(color: Colors.grey),
            ),
          ),
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text(
              'No',
              style: TextStyle(color: Colors.grey),
            ),
          ),
        ],
      ),
    ) ??
        false;
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
