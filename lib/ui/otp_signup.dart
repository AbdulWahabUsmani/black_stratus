import 'package:black_stratus/Data/ui_handler.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import './signup_details.dart';

class otp_SignUp extends StatefulWidget {
  otp_SignUp(this.email) : super();
  final String email;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return otp_SignUp_State();
  }
}

class Response {
  final bool verificationStatus;
  final String registeredEmail;
  final bool emailStatus;
  final String emailMessage;
  final String alreadyRegisteredMessage;

  Response(
      {this.verificationStatus,
      this.registeredEmail,
      this.emailStatus,
      this.emailMessage,
      this.alreadyRegisteredMessage});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
        verificationStatus: json['verificationStatus'] as bool,
        registeredEmail: json['registerdEmail'] as String,
        emailStatus: json['emailStatus'] as bool,
        emailMessage: json['emailMessage'] as String,
        alreadyRegisteredMessage: json['alreadyRegisteredMessage'] as String);
  }
}

class otp_SignUp_State extends State<otp_SignUp> {
  static var codecontroller = TextEditingController();
  static Handler handler = Handler('verifyEmail');
  static String url = handler.getUrl();

  Future<Response> codeCheckRequest() async {
    String email = widget.email;
    String code = codecontroller.text;

    if (code.isEmpty) {
      _ifempty();
    } else if (code.length != 4 || code.length > 4) {
      _codecheck();
    } else {
      var response = await http.post(Uri.encodeFull(url),
          body:
              json.encode({"userEmail": "$email", "verificationCode": "$code"}),
          headers: {
            "content-type": "application/json",
            "accept": "application/json"
          });
      var data = json.decode(response.body);
      var verificationStatus = data["verificationStatus"] as bool;

      if (verificationStatus == true) {
        setState(() {
          _ifValid();
        });
      } else {
        _ifInvalid();
      }
    }
  }

  static Handler handler2 = Handler('sendEmail');
  static String url2 = handler2.getUrl();

  Future<Response> resendCodeRequest() async {
    String email2 = widget.email;
    var response = await http.post(Uri.encodeFull(url2),
        body: json.encode({"userEmailRequest": "$email2"}),
        headers: {
          "content-type": "application/json",
          "accept": "application/json"
        });

    var data = json.decode(response.body);
    var emailMessage = data["emailMessage"] as String;
    var emailStatus = data["emailStatus"] as bool;
    var alreadyRegisteredMessage = data["alreadyRegisteredMessage"] as String;

    if (emailStatus == true) {
      setState(() {
        _ifValid1(emailMessage);
      });
    } else {
      _ifRegistered(alreadyRegisteredMessage);
    }
  }

  Widget _ifValid() {
    // flutter defined function
    String email = widget.email;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Valid code"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SignUp_details(email)));
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifInvalid() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Invalid code"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifempty() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please fill out all the fields"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifValid1(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifRegistered(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _codecheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("code must be equal to 4 characters"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            }),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/full_background.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32.0, right: 32),
            child: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate([
                    Padding(
                      padding: const EdgeInsets.only(top: 64.0),
                      child: TextFormField(
                        keyboardType: TextInputType.numberWithOptions(),
                        controller: codecontroller,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                            labelText: 'Verification code',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintText: 'Enter verification code',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            )),
                        cursorColor: Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                      child: RaisedButton(
                        child: Text('Verify',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        color: Colors.amberAccent,
                        onPressed: () {
                          codeCheckRequest();
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                      child: FlatButton(
                        child: Text('Resend verification code',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.amberAccent)),
                        onPressed: () {
                          resendCodeRequest();
                        },
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
