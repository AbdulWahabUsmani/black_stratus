import 'dart:io';
import 'package:black_stratus/Data/ui_handler.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import './login.dart';

class SignUp_details extends StatefulWidget {
  SignUp_details(this.email) : super();
  final String email;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignUp_details_State();
  }
}

class Response {
  final String user_reg_status;
  final String registeredEmail;
  final String alreadyRegisteredStatus;

  Response(
      {this.user_reg_status,
      this.registeredEmail,
      this.alreadyRegisteredStatus});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      user_reg_status: json['user_reg_status'] as String,
      registeredEmail: json['registeredEmail'] as String,
      alreadyRegisteredStatus: json['alreadyRegisteredStatus'] as String,
    );
  }
}

class SignUp_details_State extends State<SignUp_details> {
  static var designation = TextEditingController();
  static var firstname = TextEditingController();
  static var lastname = TextEditingController();
  static var mobileno = TextEditingController();
  static var password = TextEditingController();
  static var confirmpassword = TextEditingController();
  String dropdownValue;
  static Handler handler = Handler('stratusUserReg');
  static String url = handler.getUrl();

  Future<Response> makeRequest() async {
    String des = dropdownValue;
    String firsname = firstname.text;
    String lasname = lastname.text;
    String mobno = mobileno.text;
    String pass = password.text;
    String confpass = confirmpassword.text;
    String email = widget.email;

    if (des == null ||
        firsname.isEmpty ||
        lasname.isEmpty ||
        mobno.isEmpty ||
        pass.isEmpty) {
      _ifempty();
    } else if (pass.length < 8) {
      _passwordcheck();
    } else if (pass != confpass) {
      _confirmpasswordcheck();
    } else {
      var response = await http.post(Uri.encodeFull(url),
          body: json.encode({
            "first_name": "$firsname",
            "last_name": "$lasname",
            "mobile_number": "$mobno",
            "user_pwd": "$pass",
            "gender": "$des",
            "email": "$email"
          }),
          headers: {
            "content-type": "application/json",
            "accept": "application/json"
          });

      var data = json.decode(response.body);
      var user_reg_status = data["user_reg_status"] as String;

      if (user_reg_status == "Done With Stratus UserRegistration") {
        setState(() {
          _ifValid();
        });
      } else {
        _ifInvalid();
      }
    }
  }

  Widget _ifValid() {
    // flutter defined function
    String email = widget.email;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Registered sucessfully"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Login()));
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifInvalid() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Email is already Registered"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifempty() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please fill out all the fields"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _passwordcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content:
              Text("password must be greater then or equal to 8 characters"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _confirmpasswordcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("password and confirm password field doesnt matches"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/full_background.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate([
                    Padding(
                      padding: const EdgeInsets.only(top: 64.0),
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: <String>['Mr', 'Mrs', 'Mx']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              style: TextStyle(color: Colors.grey),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: TextFormField(
                        controller: firstname,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                            labelText: 'First name',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintText: 'Enter first name',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            )),
                        cursorColor: Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: TextFormField(
                        controller: lastname,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                            labelText: 'Last name',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintText: 'Enter last name',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            )),
                        cursorColor: Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: TextFormField(
                        controller: mobileno,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                            labelText: 'Cell',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintText: 'Enter mobile number',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            )),
                        cursorColor: Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: TextFormField(
                        controller: password,
                        obscureText: true,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                            fillColor: Colors.grey,
                            labelText: 'Password',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintText: 'Enter password',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            )),
                        cursorColor: Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: TextFormField(
                        controller: confirmpassword,
                        obscureText: true,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                            fillColor: Colors.grey,
                            labelText: 'Confirm Password',
                            labelStyle: TextStyle(color: Colors.grey),
                            hintText: 'Enter confirm password',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            )),
                        cursorColor: Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                      child: RaisedButton(
                        child: Text(
                          'Register',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        color: Colors.amberAccent,
                        onPressed: () {
                          makeRequest();
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                      child: RaisedButton(
                        child: Text(
                          'Cancel',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        color: Colors.amberAccent,
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        },
                      ),
                    ),
                  ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: Text('Are sure you want to cancel registration'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => exit(0),
                child: Text(
                  "Yes",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  'No',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
