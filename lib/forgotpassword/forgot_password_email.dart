import 'dart:convert';
import 'package:black_stratus/Data/ui_handler.dart';
import 'package:flutter/material.dart';
import './forgot_password_code.dart';
import 'package:http/http.dart' as http;

class forgot_password_email extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return forgot_password_email_State();
  }
}

class Response {
  final String userEmail;
  final String userRegMessage;
  final bool pwdResetStatus;

  Response({this.userEmail, this.userRegMessage, this.pwdResetStatus});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      userEmail: json['userEmail'] as String,
      userRegMessage: json['userRegMessage'] as String,
      pwdResetStatus: json['pwdResetStatus'] as bool,
    );
  }
}

class forgot_password_email_State extends State<forgot_password_email> {
  static var emailcontroller = TextEditingController();
  static Handler handler = Handler('resetPwd');
  static String url = handler.getUrl();

  Future<Response> makeRequest() async {
    String email = emailcontroller.text;

    if (email.isEmpty) {
      _ifempty();
    } else if (!email.contains("@")) {
      _emailcheck();
    } else {
      var response = await http.post(Uri.encodeFull(url),
          body: json.encode({"emailPwdResetRequest": "$email"}),
          headers: {
            "content-type": "application/json",
            "accept": "application/json"
          });

      var data = json.decode(response.body);
      var userEmail = data["userEmail"] as String;
      var userRegMessage = data["userRegMessage"] as String;
      var pwdResetStatus = data["pwdResetStatus"] as bool;

      if (pwdResetStatus == true) {
        setState(() {
          _ifValid(userEmail);
        });
      } else {
        _ifRegistered(userRegMessage);
      }
    }
  }

  Widget _ifValid(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                sendDataToSecondScreen(context);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifRegistered(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifempty() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please fill out all the fields"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _emailcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please enter a valid email"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            }),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/full_background.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32.0, right: 32.0),
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 64.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: emailcontroller,
                    style: TextStyle(color: Colors.grey),
                    decoration: InputDecoration(
                        /*labelText: 'Username',
                      labelStyle: TextStyle(color: Colors.grey),*/
                        labelText: 'Email',
                        hintText: 'Enter your email',
                        labelStyle: TextStyle(color: Colors.grey),
                        hintStyle: TextStyle(color: Colors.grey),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey)),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                        )),
                    cursorColor: Colors.grey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 8.0, left: 64.0, right: 64.0),
                  child: RaisedButton(
                    child: Text('Verify',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    color: Colors.amberAccent,
                    onPressed: () {
                      makeRequest();
                    },
                  ),
                ),
              ],
            ),
          )),
    );
  }

  void sendDataToSecondScreen(BuildContext context) {
    String email = emailcontroller.text;
    print(email);
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => forgot_password_code(email),
        ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
