import 'dart:convert';
import 'dart:io';
import 'package:black_stratus/Data/ui_handler.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../ui/login.dart';

class forgot_password_code extends StatefulWidget {
  forgot_password_code(this.email) : super();
  final String email;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return forgot_password_code_State();
  }
}

class Response {
  final bool pwdResetStatus;
  final String userEmail;
  final String userRegMessage;

  Response({
    this.pwdResetStatus,
    this.userEmail,
    this.userRegMessage,
  });

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      userEmail: json['userEmail'] as String,
      userRegMessage: json['userRegMessage'] as String,
      pwdResetStatus: json['pwdResetStatus'] as bool,
    );
  }
}

class forgot_password_code_State extends State<forgot_password_code> {
  static var emailcontroller = TextEditingController();
  static var passwordcontroller = TextEditingController();
  static var confirmpasswordcontroller = TextEditingController();
  static Handler handler = Handler('updatePwd');
  static String url = handler.getUrl();

  Future<Response> makeRequest() async {
    String email = widget.email;
    String code = emailcontroller.text;
    String password = passwordcontroller.text;
    String confpassword = confirmpasswordcontroller.text;

    if (code.isEmpty || password.isEmpty || confpassword.isEmpty) {
      _ifempty();
    } else if (code.length != 4 || code.length > 4) {
      _codecheck();
    } else if (password.length < 8) {
      _passwordcheck();
    } else if (password != confpassword) {
      _confirmpasswordcheck();
    } else {
      var response = await http.post(Uri.encodeFull(url),
          body: json.encode({
            "userEmail": "$email",
            "resetCode": "$code",
            "user_pwd": "$password"
          }),
          headers: {
            "content-type": "application/json",
            "accept": "application/json"
          });

      var data = json.decode(response.body);
      var pwdResetStatus = data["pwdResetStatus"] as bool;

      if (pwdResetStatus == true) {
        setState(() {
          _ifValid();
        });
      } else {
        _ifInvalid();
      }
    }
  }

  static Handler handler2 = Handler('resetPwd');
  static String url2 = handler2.getUrl();

  Future<Response> makeRequest2() async {
    String email2 = widget.email;

    var response = await http.post(Uri.encodeFull(url2),
        body: json.encode({"emailPwdResetRequest": "$email2"}),
        headers: {
          "content-type": "application/json",
          "accept": "application/json"
        });

    var data = json.decode(response.body);
    var userEmail = data["userEmail"] as String;
    var userRegMessage = data["userRegMessage"] as String;
    var pwdResetStatus = data["pwdResetStatus"] as bool;

    if (pwdResetStatus == true) {
      setState(() {
        _ifValidemail(userEmail);
      });
    } else {
      _ifRegistered(userRegMessage);
    }
  }

  Widget _ifValid() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Password updated successfully"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Login()));
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifInvalid() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("Code is Invalid or Expired"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifempty() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("please fill out all the fields"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _codecheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("code must be equal to 4 characters"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _passwordcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content:
              Text("password must be greater then or equal to 8 characters"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifValidemail(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _ifRegistered(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _confirmpasswordcheck() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text("password and confirm password field doesnt matches"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _widget() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            }),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/revisedlogo.png',
              width: 160,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
            )
          ],
        ),
      ),
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/full_background.png"),
                    fit: BoxFit.cover)),
            child: Padding(
              padding: const EdgeInsets.only(left: 32.0, right: 32.0),
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 64.0),
                    child: TextFormField(
                      keyboardType: TextInputType.numberWithOptions(),
                      controller: emailcontroller,
                      style: TextStyle(color: Colors.grey),
                      decoration: InputDecoration(
                          /*labelText: 'Username',
                        labelStyle: TextStyle(color: Colors.grey),*/
                          labelText: 'Verification code',
                          hintText: 'Enter verification code',
                          labelStyle: TextStyle(color: Colors.grey),
                          hintStyle: TextStyle(color: Colors.grey),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          )),
                      cursorColor: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: TextFormField(
                      keyboardType: TextInputType.numberWithOptions(),
                      controller: passwordcontroller,
                      style: TextStyle(color: Colors.grey),
                      decoration: InputDecoration(
                          /*labelText: 'Username',
                        labelStyle: TextStyle(color: Colors.grey),*/
                          labelText: 'Password',
                          hintText: 'Enter password',
                          labelStyle: TextStyle(color: Colors.grey),
                          hintStyle: TextStyle(color: Colors.grey),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          )),
                      cursorColor: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: TextFormField(
                      keyboardType: TextInputType.numberWithOptions(),
                      controller: confirmpasswordcontroller,
                      style: TextStyle(color: Colors.grey),
                      decoration: InputDecoration(
                          /*labelText: 'Username',
                        labelStyle: TextStyle(color: Colors.grey),*/
                          labelText: 'Confirm Password',
                          hintText: 'enter confirm password',
                          labelStyle: TextStyle(color: Colors.grey),
                          hintStyle: TextStyle(color: Colors.grey),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          )),
                      cursorColor: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                    child: RaisedButton(
                      child: Text('Update',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      color: Colors.amberAccent,
                      onPressed: () {
                        makeRequest();
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                    child: RaisedButton(
                      child: Text('Cancel',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      color: Colors.amberAccent,
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Login()));
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 64.0, right: 64.0),
                    child: FlatButton(
                      child: Text('Resend verification code',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.amberAccent)),
                      onPressed: () {
                        makeRequest2();
                      },
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }

//  void sendDataToSecondScreen(BuildContext context) {
//    String email = widget.email;
//    String code = econtroller.text;
//    print(code);
//    Navigator.push(
//        context,
//        MaterialPageRoute(
//          builder: (BuildContext context) => forgot_password_details(code,email),
//        ));
//  }
  Future<bool> onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: Text('Are sure you want to exit'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => exit(0),
                child: Text(
                  "Yes",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  'No',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final md = MediaQuery.of(context);
    if (md.orientation == Orientation.landscape) {
      return _widget();
    }
    return _widget();
  }
}
