import 'package:flutter/material.dart';
import './ui/landing.dart';

void main() {
  runApp(MaterialApp(
    title: 'Black Stratus',
    debugShowCheckedModeBanner: false,
    home: Landing(),
  ));
}
